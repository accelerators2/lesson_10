import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_1/constants/app_colors.dart';
import 'package:flutter_app_1/constants/app_styles.dart';
import 'package:flutter_app_1/dto/episode.dart';
import 'package:flutter_app_1/generated/l10n.dart';

import '../../episode_details/episode_details_screen.dart';

class EpisodesTile extends StatelessWidget {
  const EpisodesTile(this.episode, {Key? key}) : super(key: key);

  final Episode episode;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        gradient: const LinearGradient(
          colors: [
            AppColors.neutral2,
            AppColors.primary,
          ],
        ),
        boxShadow: const [
          BoxShadow(
            blurRadius: 2,
            spreadRadius: 1,
            offset: Offset(1, 1),
            color: AppColors.neutral2,
          ),
        ],
      ),
      padding: const EdgeInsets.all(10),
      child: InkWell(
        child: Row(
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          episode.episodeCode ?? S.of(context).noData,
                          style: AppStyles.s12w500.copyWith(
                            letterSpacing: 1.5,
                            color: AppColors.neutral1,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          episode.name ?? S.of(context).noData,
                          style: AppStyles.s16w600.copyWith(
                            height: 1.6,
                            leadingDistribution: TextLeadingDistribution.even,
                            color: AppColors.background,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Text(
                          episode.airDate ?? S.of(context).noData,
                          style: const TextStyle(
                            color: AppColors.neutral1,
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Row(
              children: const [
                Icon(
                  Icons.chevron_right,
                  color: AppColors.background,
                ),
              ],
            ),
          ],
        ),
        onTap: () {
          Navigator.of(context).push(
            CupertinoPageRoute(
              builder: (context) => EpisodeDetailsScreen(episode: episode),
            ),
          );
        },
      ),
    );
  }
}
