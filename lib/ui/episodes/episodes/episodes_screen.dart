// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter_app_1/bloc/episodes/bloc_episodes.dart';
import 'package:flutter_app_1/bloc/episodes/states.dart';
import 'package:flutter_app_1/generated/l10n.dart';
import 'package:flutter_app_1/widgets/app_alert_dialog.dart';
import 'package:flutter_app_1/widgets/app_appbar.dart';
import 'package:flutter_app_1/widgets/app_nav_bar.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'widgets/body.dart';

class EpisodesScreen extends StatelessWidget {
  const EpisodesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ColoredBox(
      color: Theme.of(context).scaffoldBackgroundColor,
      child: SafeArea(
        child: Scaffold(
          bottomNavigationBar: const AppNavBar(current: 2),
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          appBar: AppAppBar(
            title: S.of(context).episodes,
          ),
          body: Stack(
            children: [
              Positioned.fill(
                child: Column(
                  children: [
                    Expanded(
                      child: BlocBuilder<BlocEpisodes, StateBlocEpisodes>(
                        buildWhen: (prev, next) {
                          if (prev.runtimeType != next.runtimeType) {
                            return true;
                          } else {
                            final prevDataLength = prev.mapOrNull(
                              data: (state) => state.data.length,
                            );
                            final nextDataLength = next.mapOrNull(
                              data: (state) => state.data.length,
                            );
                            return prevDataLength != nextDataLength;
                          }
                        },
                        builder: (context, state) {
                          return state.map(
                            initial: (_) => const SizedBox.shrink(),
                            loading: (_) => const Center(
                              child: CircularProgressIndicator(),
                            ),
                            data: (state) => Body(data: state.data),
                            error: (state) => ErrorWidget(state.error),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
              BlocConsumer<BlocEpisodes, StateBlocEpisodes>(
                builder: (context, state) {
                  final isLoading = state.maybeMap(
                    data: (state) => state.isLoading,
                    orElse: () => false,
                  );

                  return Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: isLoading ? const LinearProgressIndicator() : const SizedBox.shrink(),
                  );
                },
                listener: (context, state) {
                  state.mapOrNull(
                    data: (state) {
                      if (state.errorMessage != null) {
                        showDialog(
                          context: context,
                          builder: (context) {
                            return AppAlertDialog(
                              title: Text(S.of(context).error),
                              content: Text(state.errorMessage!),
                            );
                          },
                        );
                      }
                    },
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
